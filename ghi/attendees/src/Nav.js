function Nav() {
    return (
<nav>
<ul className="nav">
<li className="nav-item">
    <a id="title" className="nav-link disabled" aria-disabled="true">Conference GO!</a>
  </li>
  <li className="nav-item">
    <a className="nav-link active" aria-current="page" href="http://localhost:8000/api/conferences/">Home</a>
  </li>
  <li className="nav-item">
    <a className="nav-link" href="http://localhost:8000/api/locations/">New location</a>
  </li>
  <li className="nav-item">
    <a className="nav-link" href="http://localhost:8000/api/conferences/">New conference</a>
  </li>
  <li className="nav-item">
    <a className="nav-link" href="http://localhost:8000/api/conferences/3/presentations/">New presentation</a>
  </li>
</ul>
</nav>
);
}

export default Nav;