import Nav from './Nav';
import ConferenceForm from './ConferenceForm'
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';

function App(props) {
  if (props.attendees === undefined) {
    return null 
  } else {
  return (
<>
<Nav />
  <div className="container">
    <ConferenceForm />
    {/* < LocationForm /> */}
    {/* <AttendeesList attendees={props.attendees}/> */}
  </div>
</>
)}};

export default App;
