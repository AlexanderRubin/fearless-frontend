

function createCard(name, description, pictureUrl, dateStart, dateEnd, locationName) {
    const formattedStart = new Date(dateStart).toLocaleDateString()
    const formattedEnd = new Date(dateEnd).toLocaleDateString()

    return `
    <div class="card mb-4 shadow-lg">
      <img alt="..." src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
          ${formattedStart} - ${formattedEnd}
       </div>
    </div>
  `;
}

function createHorizontalCard(name, description, pictureUrl, dateStart, dateEnd, locationName) {

    const formattedStart = new Date(dateStart).toLocaleDateString()
    const formattedEnd = new Date(dateEnd).toLocaleDateString()
    return `
    <div class="col">
      <div class="card mb-4 shadow">
        <img alt="..." src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${formattedStart}-${formattedEnd}
        </div>
      </div>
    </div>
    `;
  }



window.addEventListener('DOMContentLoaded', async () => {
    
    const url = 'http://localhost:8000/api/conferences/'
    
    try {
        const response = await fetch(url)
        if (!response.ok) {
            return alert(
                `${response.status}: ${response.url} ${response.statusText}`)
        } else {
            
            const data = await response.json()
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {
                    const details = await detailResponse.json()
                    const name = details.conference.name
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url
                    const dateStart = details.conference.starts
                    const dateEnd = details.conference.ends
                    const locationName = details.conference.location.name
                    const html = createCard(name, description, pictureUrl, dateStart, dateEnd, locationName)
                    const column = document.querySelector('.col')
                    column.innerHTML += html
                }
            }
        }
    } catch (err){
        return alert(
            `${"There was an unknown error that occurred"}`
          );
    }
})